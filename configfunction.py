#!/usr/bin/env python3
# 0)MailServerlogin
# 1)Email mit der Gesendet werden soll
# 2)Passwort fur den serverloginkl
# 3)Server IP / Hostname
# 4)Server PORT
# 5)Datenbank IP
# 6)Datenbank Database
# 7)Datenbank Table
# 8)Datenbank User
# 9)Datenbank Password
# 10)Static Path zu dem PDF Ordner
# 11)admin support email
import ctypes

def initialize_configuration():
    fh = open('./config/config.config')
    static_configuration = [line.rstrip() for line in fh.readlines()]
    fh.close()
    return static_configuration


def write_config(static_config_variables):
    file = open("./config/config.config", 'w')
    for config_var in static_config_variables:
        file.write(config_var + "\n")
    file.close()


def initialize_configGUI(object_list):
    for data in range(0, len(static_config_variables)):
        object_list[data].setText(str(static_config_variables[data]))


def save_config(object_list):
    for data in range(0, len(object_list)):
        static_config_variables[data] = object_list[data].text()
        write_config(static_config_variables)

try:
    static_config_variables = initialize_configuration()
except:
    ctypes.windll.user32.MessageBoxW(0,u"Konfigurationsdatei fehlt!\n\nBitte ./config/config.ini überprüfen.",u"Error!", 0)
