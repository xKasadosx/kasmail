import sys
import os
from ui import update
import ui
import ctypes

from PyQt5.QtWidgets import QDialog
from qtpy import QtWidgets
from ui import mainwindow
from ui.mainwindow import Ui_MainWindow
import configGUI as configGUI
import databaseGUI as databaseGUI
import logfilterGUI as logfilterGUI
from kasmailfunctions import *
import kasmailfunctions as kasmailfunctions
import configfunction as configfunction
import logfunction as logfunction
import infoGUI as infoGUI
import updateGUI as updateGUI
import time

os.environ['QT_API'] = 'pyqt'


#########################
###just comfy thingy ####
#########################
# from qtpy import uic
# uic.compileUiDir("ui")
#########################

def send_email():
    kasmailfunctions.send_email(scrollAreaList, invoice_directory)


def reload_files():
    kasmailfunctions.reload_files(scrollAreaList, invoice_directory)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()

        self.updateWindow = updateGUI.UpdateWindow()
        self.infoWindow = infoGUI.InfoWindow()
        self.databasecustomerWindow = databaseGUI.DatabasecustomerWindow()
        self.configWindow = configGUI.ConfigWindow(self)
        self.logWindow = logfilterGUI.LogWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        global invoice_directory
        invoice_directory = configfunction.static_config_variables[10]

        self.ui.buttonSend.clicked.connect(send_email)
        self.ui.buttonUpdate.clicked.connect(reload_files)
        self.ui.buttonClose.clicked.connect(self.close_window)
        self.ui.buttonDatenbank.clicked.connect(self.open_databaseWindow)
        self.ui.menuDataConfiguration.triggered.connect(self.open_configWindow)
        self.ui.menuDataClose.triggered.connect(self.close_window)
        self.ui.menuDatabaseOverview.triggered.connect(self.open_databaseWindow)

        self.ui.menuLogsSearchlog.triggered.connect(self.open_logSearchWindow)
        self.ui.menuLogsLastlog.triggered.connect(logfunction.open_latest_log)
        self.ui.menuInfoInfo.triggered.connect(self.open_infoWindow)
        self.ui.menuInfoUpdate.triggered.connect(self.open_updateLog)
        self.ui.scrollAreaList.doubleClicked.connect(self.open_pdf_from_list)
        global scrollAreaList
        scrollAreaList = self.ui.scrollAreaList

    def close_window(self):
        self.close()

    def open_configWindow(self):
        self.configWindow.show()

    def open_databaseWindow(self):
        self.databasecustomerWindow.show()

    def open_logSearchWindow(self):
        self.logWindow.show()

    def open_infoWindow(self):
        self.infoWindow.show()

    def open_updateLog(self):
        self.updateWindow.show()

    def open_pdf_from_list(self):
        try:
            selectedPDF = self.ui.scrollAreaList.currentItem().text()
            os.startfile(invoice_directory + "/" + str(selectedPDF))
        except:
            ctypes.windll.user32.MessageBoxW(0, u"Datei kann nicht geöffnet werden oder wurde nicht gefunden!\n\nBitte überprüfen!", u"Error!", 0)


try:
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    Design = open("./config/Combinear.qss", 'r')

    with Design:
        qss = Design.read()
        app.setStyleSheet(qss)

    reload_files()
    window.show()

except:
    ctypes.windll.user32.MessageBoxW(0, u"Theme Fehlt!\n ./config/Combinear.qss", u"Error!", 0)
finally:
    sys.exit(app.exec_())
