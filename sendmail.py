#!/usr/bin/env python3

import subprocess
import smtplib
import socket
import configfunction as configconstants
import os
import sys
import logfunction as logfunction
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.utils import formatdate
from email import encoders

def send_email_to_office(invoice_directory, invoice_attachment, customerMail):
    #fügt nur mehrere emails hinzu (CC) wenn diese vorhanden sind.
    print("___")
    cc_flag = ""
    cc = ""
    if customerMail[1] != "":
        cc = customerMail[1]
        cc_flag = True
    if customerMail[2] != "":
        cc = cc + "," + customerMail[2]
        cc_flag = True

    msg = MIMEMultipart()

    if len(invoice_attachment) == 1 and invoice_attachment[0].startswith("ar-") \
            or len(invoice_attachment) == 1 and invoice_attachment[0].startswith("ag-"):
        file = invoice_attachment[0].split("-")
        msg['Subject'] = 'Frachtrechnung: ' + str(file[3])
    else:
        msg['Subject'] = 'Frachtrechnung'

    msg['From'] = configconstants.static_config_variables[1]
    msg['To'] = customerMail[0]
    msg['Cc'] = cc
    bcc = "BCC MAIL ADRESS"

    for name in invoice_attachment:
        if name.startswith('ar-') or name.startswith('ag-'):
            bcc = ""


    try:
        #liest email.txt ein um Email mit Text zu versenden.
        with open('./config/email.txt', encoding='iso-8859-15') as file:
            mailText = file.read().strip()
        msg.attach(MIMEText(mailText, 'html'))
    except:
        #errorbox writing
        logfunction.error_log("email.txt Datei fehlt.")
        return 0


    #####
    #fügt die PDFS der Email hinzu
    for file in invoice_attachment:
        file_path = invoice_directory + file
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(file_path, 'rb').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(file_path)))
        msg.attach(part)

    try: #wenn TTLS gewünscht ist und mit "yes" in der config vorhanden ist.
        if configconstants.static_config_variables[12] == "yes":
            server = smtplib.SMTP(configconstants.static_config_variables[3], configconstants.static_config_variables[4])
            server.starttls()
            server.login(configconstants.static_config_variables[0], configconstants.static_config_variables[2])
        else:
            server = smtplib.SMTP(configconstants.static_config_variables[3])
        text = msg.as_string()
        if cc_flag:
            server.sendmail(configconstants.static_config_variables[0], [customerMail[0], cc, bcc], text)
        else:
            server.sendmail(configconstants.static_config_variables[0], [customerMail[0], bcc], text)
        server.quit()
    except (socket.gaierror, socket.error, socket.herror, smtplib.SMTPException) as e:
        #errorbox writing
        logfunction.error_log(e)
        return 0
    return 1
