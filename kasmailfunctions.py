import os
from PyQt5.QtCore import Qt

from PyQt5.QtWidgets import QListWidgetItem
from configfunction import static_config_variables as configconstants
import logfunction as logfunction
import databasefunction as databasefunction
import sendmail as sendmailfunction

# file to send directory
invoices_directory = os.getcwd()  # initaldirectory
backup_invoices_directory = invoices_directory + "/sent/"  # gesendet directory
log_directory = invoices_directory + "/logs/"  # log direcory
label_input = ""


def create_log_file():
    logDir = invoices_directory + "/logs/"  # log direcory
    sentLogfile = logDir + str(logfunction.create_timestamp("%Y-%m-%d-%H%M.txt"))
    return sentLogfile


def reload_files(scrollAreaList, invoice_directory):
    files_from_invoice_directory = os.listdir(invoice_directory)
    scrollAreaList.clear()
    for file in files_from_invoice_directory:
        if file.startswith('ar') or file.startswith('ag') or file.startswith('Ausfuhrb-') and file.endswith('.pdf') \
                or file.startswith('csv-') and file.endswith('.csv'):
            item = QListWidgetItem(file)
            item.setTextAlignment(Qt.AlignHCenter)
            scrollAreaList.addItem(item)


def filter_for_invoices(invoice_directory):
    invoice_list = []
    # durchsucht ordner nach files mit "ar | ag"
    for invoice in invoice_directory:
        if invoice.startswith('ar') or invoice.startswith('ag') and invoice.endswith('.pdf'):
            # cuttet die ID aus dem Filename heraus
            filename_split = invoice.split("-")
            id_from_split = filename_split[2]
            # fuegt ID in die liste ein
            invoice_list.append(id_from_split)

        if invoice.startswith('Ausfuhrb-') and invoice.endswith('.pdf') \
                or invoice.startswith('csv-') and invoice.endswith('.csv'):
            filename_split = invoice.split("-")
            filename_split = filename_split[1].split(".")
            id_from_split = filename_split[0]

            # fuegt ID in die liste ein
            invoice_list.append(id_from_split)
    # loescht doppelte IDs, sortiert sie und erkennt die anzahl an verschiedenen IDs
    invoice_list = list(dict.fromkeys(invoice_list))
    invoice_list.sort()
    return invoice_list


def send_email(scrollAreaList, invoice_directory):
    global customer_id_exists, db_customer_mail
    file_directory = os.listdir(invoice_directory)
    mails_to_send_counter = 0
    path_with_invoices = invoice_directory + "/"
    mails_sent_counter = 0
    sent_log_file = create_log_file()
    file_list = filter_for_invoices(file_directory)

    # durchlaeuft alle ids aus der sortierten Liste
    for invoice_customer_id in file_list:
        # print(id)
        # liste nur fuer eine id lang
        invoice_list = []
        for files in file_directory:
            if files.startswith(str(invoice_customer_id), 7) or files.startswith(
                    'Ausfuhrb-' + str(invoice_customer_id)) and files.endswith(
                    '.pdf') or files.startswith('csv-' + str(invoice_customer_id)) and files.endswith('.csv'):
                db_customer_id = databasefunction.find_data_from_customer(invoice_customer_id)
                invoice_cutted_id = invoice_customer_id.lstrip("0")
                # print(cuttedID)
                if db_customer_id[0] == invoice_cutted_id:
                    customer_id_exists = True
                    invoice_list.append(files)
                    mails_to_send_counter = mails_to_send_counter + 1
                else:
                    # macht dann nichts.
                    customer_id_exists = False
        # verbindet sich fuer jede ID mit der Datenbank, um die Email Adressen herauszulesen
        if customer_id_exists:
            db_customer_mail = databasefunction.get_email_from_pdf(invoice_customer_id)
            # print(customerMailFromDatabase)

        # splittet die filelist in einzelne parts auf um nur X Rechnungen pro Mail zu senden.
        if invoice_customer_id != "":
            max_counter = databasefunction.get_max_pdf_to_send(invoice_customer_id)
            while len(invoice_list) != 0:
                pdf_send = invoice_list[:max_counter]
                invoice_list = invoice_list[max_counter:]
                mail_sent = sendmailfunction.send_email_to_office(path_with_invoices, pdf_send, db_customer_mail)
                if mail_sent:
                    mails_sent_counter = mails_sent_counter + 1
                    # Verschiebt die Dateien in den "Sent" Ordner und erstellt eine Logfile an wen die PDFS gesendet wurden
                    sent_logfile_open = logfunction.backup_to_sent_folder(pdf_send, db_customer_mail
                                                                          , str(invoice_customer_id), invoice_directory,
                                                                          sent_log_file)

    # Gibt als Label an ob emails versendet wurden oder nicht
    if mails_to_send_counter >= 1:
        ### label mit wurde gversendet !!!!!!!!!!!!!!!!!!!!###############
        logfunction.open_log_file(sent_logfile_open)
        reload_files(scrollAreaList, invoice_directory)
    else:
        #######WURDE NICHT VERSENDET
        print("")


####### LISTBOX / PDF FUNCTIONS#######
def initial_dir():
    global invoices_directory
    invoices_directory = databasefunction.static_config_variables[10]
    return invoices_directory
