import os
import time
import datetime
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog
from qtpy import QtWidgets
from PyQt5.QtWidgets import QListWidgetItem

workingDir = os.getcwd()
sent_directory = workingDir + "/logs/"


def apply_filter(object_list, list_view):
    global end_datum, start_datum
    list_view.clear()
    files = os.listdir(sent_directory)
    countLogs = 0
    selected_client = object_list[0].text()
    selected_start_datum = str(object_list[1].date().toPyDate())
    selected_end_datum = str(object_list[2].date().toPyDate())

    if selected_start_datum != "2000-01-01":
        split_start = selected_start_datum.split("-")
        start_datum = split_start[0] + split_start[1] + split_start[2]

    if selected_end_datum != "2000-01-01":
        split_end = selected_end_datum.split("-")
        end_datum = split_end[0] + split_end[1] + split_end[2]

    files.sort()
    logfile_list = []
    for file in reversed(files):
        file_name = str(sent_directory) + "/" + str(file)

        modified_date = os.path.getmtime(file_name)
        modified_date = time.strftime('%Y%m%d', time.localtime(modified_date))

        if selected_start_datum != "" and selected_end_datum != "":
            if start_datum <= modified_date <= end_datum:
                logfile_list.append(file)

        elif selected_start_datum != "" and selected_end_datum == "":
            if modified_date >= start_datum:
                logfile_list.append(file)

        elif selected_start_datum == "" and selected_end_datum != "":
            if modified_date <= end_datum:
                logfile_list.append(file)
        else:
            logfile_list.append(file)

    for file in logfile_list:
        file_name = str(sent_directory) + "/" + str(file)

        if selected_client != "":
            with open(file_name, 'r') as content:
                save = content.read()
                if selected_client.lower() in save.lower():
                    item = QListWidgetItem(file)
                    item.setTextAlignment(Qt.AlignHCenter)
                    list_view.addItem(item)
                    countLogs += 1
        if selected_client == "":
            item = QListWidgetItem(file)
            item.setTextAlignment(Qt.AlignHCenter)
            list_view.addItem(item)
            countLogs += 1

    # labelInfoText.config(text="Es wurden "+ str(countLogs) + " Logs gefunden." ,foreground="yellow")


def open_log_file(list_view):
    selected_log = list_view.currentItem().text()
    os.startfile(sent_directory + "/" + str(selected_log))
