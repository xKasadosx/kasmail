import sys
from PyQt5.QtWidgets import QDialog, QLineEdit
from qtpy import QtWidgets
from ui.info import Ui_info


class InfoWindow(QtWidgets.QMainWindow, Ui_info):
    def __init__(self, parent=None):
        super(InfoWindow, self).__init__()
        self.setupUi(self)
        self.setWindowTitle("Info")

        self.infoButtonClose.clicked.connect(self.close_window)

    def close_window(self):
        self.close()
