from os import supports_effective_ids
import sys
from PyQt5.QtWidgets import QDialog, QLineEdit
from qtpy import QtWidgets
from ui.configwindow import Ui_configwindow
import configfunction as configfunction


def save_data():
    configfunction.save_config(object_list)
    configfunction.initialize_configGUI(object_list)


def set_default():
    configfunction.initialize_configGUI(object_list)


class ConfigWindow(QtWidgets.QMainWindow, Ui_configwindow):
    def __init__(self, parent=None):
        super(ConfigWindow, self).__init__()
        self.setupUi(self)
        self.setWindowTitle("Konfiguration")

        self.buttonConfigClose.clicked.connect(self.close_window)
        self.buttonConfigDefault.clicked.connect(set_default)
        self.buttonConfigSave.clicked.connect(save_data)

        self.tabSMTPLineSMTPUserPassword.setEchoMode(QLineEdit.Password)
        self.tabDatabaseLineDatabasePassword.setEchoMode(QLineEdit.Password)

        global object_list
        object_list = [self.tabSMTPLineSMTPUser, self.tabSMTPLineSMTPMailadress, self.tabSMTPLineSMTPUserPassword,
                       self.tabSMTPLineSMTPServerIP, self.tabSMTPLineSMTPServerPort, self.tabDatabaseLineDatabaseIP,
                       self.tabDatabaseLineDatabaseName, self.tabDatabaseLineDatabaseTable,
                       self.tabDatabaseLineDatabaseUser,
                       self.tabDatabaseLineDatabasePassword, self.tabStaticLinePath, self.tabStaticLineEmail,
                       self.tabSMTPLineSMTPTTLS]

        configfunction.initialize_configGUI(object_list)

    def close_window(self):
        self.close()
