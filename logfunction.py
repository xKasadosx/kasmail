import os
from datetime import datetime
import time
import glob
import databasefunction as databasefunction
import ctypes

invoice_directory = os.getcwd()
sent_invoice_directory = invoice_directory + "/sent/"  # gesendet directory
log_directory = invoice_directory + "/logs/"  # log direcory
error_directory = invoice_directory + "/errorlog/"
error_log_file = invoice_directory + "/errorlog/errorlog.txt"
error_database_log_file = invoice_directory + "/errorlog/databaselog.txt"


def create_timestamp(time_stamp_format):
    timestamp = time.strftime(time_stamp_format)
    return timestamp


def create_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def create_file_if_not_exist(file):
    file = open(file, 'a+')
    file.close


def open_log_file(sent_log_file):
    os.startfile(sent_log_file)


###change to label instead of txt document###
def open_latest_log():
    try:
        list_of_files = glob.glob(log_directory + "*")
        latest_file = max(list_of_files, key=os.path.getctime)
        os.startfile(latest_file)
    except:
        ctypes.windll.user32.MessageBoxW(0, u"Letzter Log konnte nicht geöffnet/gefunden werden!\n\n-> Ordner './logs' überprüfen.", u"Error!", 0)


def error_log(error):
    create_directory(error_directory)
    create_file_if_not_exist(error_log_file)

    with open(error_log_file, 'r') as contents:
        save = contents.read()
    with open(error_log_file, 'w') as contents:
        contents.write(str(create_timestamp("%Y-%m-%d-%H:%M")) + "\n" + str(error) + "\n\n")
    with open(error_log_file, 'a') as contents:
        contents.write(save)


def database_log(database_action, customer_id, customer_name, customer_mail_one, customer_mail_two, customer_mail_three, customer_max_pfds):
    create_directory(error_directory)
    create_file_if_not_exist(error_database_log_file)

    with open(error_database_log_file, 'r') as contents:
        save = contents.read()
    with open(error_database_log_file, 'w') as contents:
        contents.write(str(create_timestamp(
            "%Y-%m-%d-%H:%M")) + "\nAktion: " + database_action + ", ID: " + customer_id + ", NAME: " + customer_name + ", MAIL1: " + customer_mail_one + ", MAIL2: " + customer_mail_two + ", MAIL3: " + customer_mail_three + "Rechnung/Email: " + str(
            customer_max_pfds) + "\n\n")
    with open(error_database_log_file, 'a') as contents:
        contents.write(save)


def email_send_log(sent_log_file, customer_Id, db_customer_mail, attachment_counter, pdf_log):
    customer = databasefunction.find_data_from_customer(customer_Id)
    file = open(sent_log_file, 'a+')
    file.write("E-Mail gesendet an: " + str(customer[1]) + " | ID: " + str(customer_Id) + " | Mailadd.: ")
    mail = ', '.join(db_customer_mail)
    file.write(str(mail) + " ")
    file.write(" | Anhaenge: " + str(attachment_counter) + "\n")
    file.write(str(pdf_log) + "_" * 60 + "\n")
    file.close


# ueberprueft auf doppelten file name im gesendet ordner
# falls doppelt setzt er hinter die ID "-HH-MM" um das doppelte file zu umgehen.
def check_for_doubled_files(sent_files_directory, file):
    if os.path.exists(sent_files_directory + file):
        timestamp = time.strftime("%H-%M")
        file = file.split(".")
        new_file = file[0] + "-" + timestamp + "." + file[1]
        print(new_file)
        return new_file
    else:
        return file


# Verschieben der Daten in "Sent" und schreibt Logfile
def backup_to_sent_folder(invoice_files, db_customer_mail, customer_Id, directoryWithFiles, sent_log_file):
    sent_files_directory = sent_invoice_directory + str(create_timestamp("%Y-%m-%d/"))

    # sentLogfile = logDir + str(create_timestamp("%Y-%m-%d-%H%M.txt"))

    # erstellt ordner falls nicht vorhanden (gesendete pdfs)
    create_directory(sent_files_directory)
    # erstellt ordner falls nicht vorhanden (logfile ordner)
    create_directory(log_directory)
    # verschiebt gesendete pdfs in den gesendet ordner und speichert die Anzahl der verschobenen Daten
    attachment_counter = 0
    pdf_log = ""
    for file in invoice_files:
        path_with_file = directoryWithFiles + "/" + file
        file = check_for_doubled_files(sent_files_directory, file)
        os.rename(path_with_file, sent_files_directory + file)
        attachment_counter = attachment_counter + 1
        pdf_log = pdf_log + str(file) + "\n"
    # oeffnet das Logfile und schreibt die ID, Mail, Anzahl Anhaenge ins File.
    email_send_log(sent_log_file, customer_Id, db_customer_mail, attachment_counter, pdf_log)
    return sent_log_file
