import sys
from tkinter.constants import CURRENT
from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QDateEdit, QDateTimeEdit, QDialog
from qtpy import QtWidgets
from ui.logsearch import Ui_Logsearch
import logfilterfunction as logfilterfunction
from datetime import date

today = date.today()
date = today.strftime("%d-%m-%Y")
date = date.split("-")
end_date = QDate(int(date[2]), int(date[1]), int(date[0]))
start_date = QDate(int(date[2]) - 2, int(date[1]), int(date[0]))


def apply_filter():
    logfilterfunction.apply_filter(object_list, list_view)


def get_selected_file():
    logfilterfunction.open_log_file(list_view)


class LogWindow(QtWidgets.QMainWindow, Ui_Logsearch):
    def __init__(self, parent = None):
        super(LogWindow,self).__init__()
        self.setupUi(self)
        self.setWindowTitle("Logs")

        self.logsearchButtonSearchFor.clicked.connect(apply_filter)
        self.logsearchButtonStartDate.setDate(start_date)
        self.logsearchButtonEndDate.setDate(end_date)

        global object_list
        object_list = [self.logsearchLineSearchfor, self.logsearchButtonStartDate, self.logsearchButtonEndDate]
        global list_view
        list_view = self.logsearchListView
        self.logsearchButtonEndDate.setDate
        self.logsearchListView.doubleClicked.connect(get_selected_file)

    def close_window(self):
        self.close()
