#!/usr/bin/env python3
from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem
import pymssql
from configfunction import *
from configfunction import static_config_variables
import databaseGUI
import logfunction as logfunction
import kasmailfunctions as kasmailfunctions
from databaseGUI import *
from qtpy import QtWidgets

database_table = static_config_variables[7]


# CONNECT TO DATABASE4
def connect_to_database():
    global database_connection
    database_connection = pymssql.connect(
        server=static_config_variables[5],
        database=static_config_variables[6],
        user=static_config_variables[8],
        password=static_config_variables[9],
        login_timeout=2
    )


def select_customer_data_column(tableview, object_list):
    clear_database_gui(object_list)
    row = tableview.currentRow()
    ID = tableview.item(row, 0).text()
    db_customer_data = find_data_from_customer(ID)
    clear_database_gui(object_list)
    for item in range(0, len(object_list)):
        object_list[item].setText(str(db_customer_data[item]))


def clear_database_gui(object_list):
    for item in object_list:
        item.clear()


def find_all(customer_name, tableView):
    customer_name = customer_name.text()
    customer_tuple = find_all_customer(customer_name)
    if customer_tuple != 0:
        databaseGUI.tab.setCurrentIndex(1)
        insert_into_all_customer(customer_tuple, tableView)


def insert_into_all_customer(customer_tuple, tableView):
    for row_number, row_data in enumerate(customer_tuple):
        tableView.insertRow(row_number)
        for column_number, data in enumerate(row_data):
            tableView.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))


def insert(object_list, customer_info, tab_search_label_found):
    customer_info = get_customer_info_from_textbox(object_list, customer_info)
    if (customer_info[0] != "" and customer_info[1] != "" and customer_info[2] != "" and customer_info[5] != ""
            and int(customer_info[5]) > 0):

        is_inserted = insert_new_customer(customer_info[0], customer_info[1], customer_info[2],
                                          customer_info[3], customer_info[4], customer_info[5])

        if is_inserted:
            tab_search_label_found.setText(
                str(customer_info[1]) + "\nID: " + str(customer_info[0]) + "\nWurde hinzugefügt")
            clear_database_gui(object_list)
    else:
        tab_search_label_found.setText(
            str(customer_info[1]) + "\nID: " + str(customer_info[0]) + "\nKonnte nicht hinzugefügt werden.")


def update(objectList, customer_info, tabSearchLabelFound):
    customer_info = get_customer_info_from_textbox(objectList, customer_info)
    customer_is_updated = update_customer(customer_info[0], customer_info[1], customer_info[2],
                                          customer_info[3], customer_info[4], customer_info[5])
    if customer_is_updated:
        tabSearchLabelFound.setText(str(customer_info[1]) + "\nID: " + str(customer_info[0]) + "\nWurde bearbeitet.")


def delete(object_list, customer_info, tabSearchLabelFound):
    customer_info = get_customer_info_from_textbox(object_list, customer_info)
    customer_is_deleted = delete_customer(customer_info[0], customer_info[1], customer_info[2], customer_info[3],
                                          customer_info[4], customer_info[5])
    if customer_is_deleted:
        tabSearchLabelFound.setText(str(customer_info[1]) + "\nID: " + str(customer_info[0]) + "\nWurde gelöscht.")
        clear_database_gui(object_list)


def find(object_list, tabSearchLabelFound):
    customer_id = object_list[0].text()
    db_customer_data = find_data_from_customer(customer_id)
    if customer_id is not None and db_customer_data[1] is not None and db_customer_data[3] is not None:
        clear_database_gui(object_list)
        for item in range(0, len(object_list)):
            object_list[item].setText(str(db_customer_data[item]))
        tabSearchLabelFound.setText(
            str(db_customer_data[1]) + "\nID: " + str(db_customer_data[0]) + "\nWurde gefunden.")
    else:
        clear_database_gui(object_list)
        object_list[0].setText(customer_id)
        tabSearchLabelFound.setText("Konnte nicht gefunden werden.")


def get_customer_info_from_textbox(object_list, customer_info):
    for item in range(0, len(object_list)):
        customer_info[item] = object_list[item].text()
    return customer_info

####abfragen####

def insert_new_customer(customer_id, customer_name, customer_mail_one, customer_mail_two,
                        customer_mail_three, customer_max_pdfs):

    try:
        connect_to_database()
    except (pymssql.Error, pymssql.Warning) as error_message:
        databaseGUI.tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
        logfunction.error_log(error_message)
        return False

    try:
        db_insert_query = "insert_customer @customer_id = %s, @customer_name = '%s' , @customer_mailone = '%s', " \
                          "@customer_mailtwo = '%s', @customer_mailthree = '%s' , @customer_sendpdfs = %s " % \
                          (customer_id, customer_name, customer_mail_one, customer_mail_two, customer_mail_three,
                           customer_max_pdfs)

        db_cursor = database_connection.cursor()
        db_cursor.execute(db_insert_query)
        database_connection.commit()
        logfunction.database_log("Eingefügt", customer_id, customer_name, customer_mail_one,
                                 customer_mail_two,
                                 customer_mail_three, customer_max_pdfs)
        return True
    except (pymssql.Error, pymssql.Warning):
        return False
    finally:
        database_connection.close()

def update_customer(customer_id, customer_name, customer_mail_one, customer_mail_two,
                    customer_mail_three, customer_max_pdfs):
    try:
        connect_to_database()
    except (pymssql.Error, pymssql.Warning) as error_message:
        databaseGUI.tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
        logfunction.error_log(error_message)
        return 0


    # holt sich die alten daten und schreibt sie als log vor dem Update
    if customer_id != "":
        db_customer_id, db_customer_name, db_customer_mail_one, db_customer_mail_two \
            , db_customer_mail_three, db_customer_max_pdfs = find_data_from_customer(customer_id)

        if customer_id == db_customer_id:
            if (
                    customer_id != "" and customer_name != ""
                    and customer_mail_one != ""
                    and customer_max_pdfs != ""
                    and int(customer_max_pdfs) > 0):

                try:
                    db_update_query = "update_customer @customer_id = %s, @customer_name = '%s', " \
                                      "@customer_mailone = '%s' , @customer_mailtwo = '%s' , @customer_mailthree = '%s'" \
                                      ", @customer_sendpdfs = %s" % (customer_id, customer_name, customer_mail_one,
                                                                     customer_mail_two,
                                                                     customer_mail_three,
                                                                     customer_max_pdfs)

                    db_cursor = database_connection.cursor()
                    db_cursor.execute(db_update_query)
                    database_connection.commit()

                    logfunction.database_log("Vor-Update", db_customer_id, db_customer_name,
                                             db_customer_mail_one,
                                             db_customer_mail_two, db_customer_mail_three,
                                             db_customer_max_pdfs)

                    logfunction.database_log("Nach-Update", customer_id, customer_name, customer_mail_one,
                                             customer_mail_two,
                                             customer_mail_three, customer_max_pdfs)
                    return 1

                except (pymssql.Error, pymssql.Warning) as error_message:
                    logfunction.error_log(error_message)
                    tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
                    return 0
                finally:
                    database_connection.close()
            else:
                logfunction.error_log("DB: Update - Nicht alle Angaben angegeben (mind. Id, Name, Mail1) Angegeben!")
        else:
            logfunction.error_log("DB: Update - Nicht alle Angaben angegeben (mind. Id, Name, Mail1) Angegeben!")


def delete_customer(db_customer_id, db_customer_name, db_customer_mail_one, db_customer_mail_two,
                    db_customer_mail_three, db_customer_max_pdfs):
    try:
        connect_to_database()
    except (pymssql.Error, pymssql.Warning) as error_message:
        databaseGUI.tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
        logfunction.error_log(error_message)
        return 0

    if db_customer_id != "":
        customer_id_find, customer_name_find, customer_mail_one_find, customer_mail_two_find \
            , customer_mail_three_find, customer_max_pdfs_find = find_data_from_customer(db_customer_id)
        if customer_id_find is None:
            return 0
        if db_customer_id != "" and db_customer_name != "" and db_customer_mail_one != "":
            if (
                    db_customer_id and customer_id_find and db_customer_name == customer_name_find
                    and db_customer_mail_one == customer_mail_one_find
                    and db_customer_mail_two == customer_mail_two_find
                    and db_customer_mail_three == customer_mail_three_find
                    and str(db_customer_max_pdfs) == str(customer_max_pdfs_find)):
                try:
                    db_delete_query = "delete_customer @customer_id = %s" % db_customer_id
                    db_cursor = database_connection.cursor()
                    db_cursor.execute(db_delete_query)
                    database_connection.commit()

                    logfunction.database_log("Gelöscht", db_customer_id, db_customer_name, db_customer_mail_one,
                                             db_customer_mail_two,
                                             db_customer_mail_three, db_customer_max_pdfs)
                    return 1

                except (pymssql.Error, pymssql.Warning) as errorMessage:
                    logfunction.error_log(errorMessage)
                    tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
                    return 0
                finally:
                    database_connection.close()
            else:
                logfunction.error_log("DB: Loeschen - Falsche ID und Falsche Daten genutzt")
        else:
            logfunction.error_log("DB: Loeschen - Nicht alle Angaben angegeben (mind. Id, Name, Mail1) Angegeben!")
            return 0


# works + errorhandling
def find_data_from_customer(customer_id):
    try:
        connect_to_database()
    except (pymssql.Error, pymssql.Warning) as error_message:
        databaseGUI.tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
        logfunction.error_log(error_message)
        return None, None, None, None, None, None

    if customer_id != "":
        try:
            db_select_query = "get_data_from_customer @customer_id = %s " % customer_id
            db_cursor = database_connection.cursor()
            db_cursor.execute(db_select_query)
            result = db_cursor.fetchone()

            if result is not None:
                customer_id = result[0]
                customer_name = result[1]
                customer_mail_one = result[2]
                if result[3] is not None:
                    customer_mail_two = result[3]
                else:
                    customer_mail_two = ""
                if result[4] is not None:
                    customer_mail_three = result[4]
                else:
                    customer_mail_three = ""
                customer_max_pdfs = result[5]
                object_list = [customer_id, customer_name, customer_mail_one, customer_mail_two, customer_mail_three,
                               customer_max_pdfs]
                return object_list
            else:
                return None, None, None, None, None, None

        except (pymssql.Error, pymssql.Warning) as error_message:
            logfunction.error_log(error_message)
            tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
            return 0
        finally:
            database_connection.close()
    else:
        logfunction.error_log("DB: FINDE ID - Keine ID Angegeben!")
        return None, None, None, None, None


def get_max_pdf_to_send(customer_id):
    try:
        connect_to_database()
        db_max_pdf_query = "get_max_pdfs @customer_id = %s" % customer_id
        db_query = database_connection.cursor()
        db_query.execute(db_max_pdf_query)
        result = db_query.fetchone()

        if result is not None:
            return result[0]
        else:
            return None

    except (pymssql.Error, pymssql.Warning) as errorMessage:
        logfunction.error_log(errorMessage)
        return 0
    finally:
        database_connection.close()


def get_email_from_pdf(customer_id):
    try:
        connect_to_database()
    except (pymssql.Error, pymssql.Warning) as errorMessage:
        tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
        logfunction.error_log(errorMessage)
        return 0
    if customer_id != "":
        try:
            db_mail_query = "get_emailaddress @customer_id = %s" % customer_id
            db_cursor = database_connection.cursor()
            db_cursor.execute(db_mail_query)
            result = db_cursor.fetchone()

            if result is not None:
                return result
            else:
                return None

        except (pymssql.Error, pymssql.Warning) as errorMessage:
            logfunction.error_log(errorMessage)
            return 0
        finally:
            database_connection.close()
    else:
        logfunction.error_log("Keine PDF Nummer gefunden - wärend send-email vorgang")
        return 0


# neue funktion
def find_all_customer(customer_name):
    try:
        connect_to_database()
    except (pymssql.Error, pymssql.Warning) as error_message:
        databaseGUI.tabSearchLabelFound.setText("Datenbank ist nicht Erreichbar\nMelden Sie sich beim Administrator!")
        logfunction.error_log(error_message)
        return 0

    try:
        db_all_customer_query = "get_all_customer @customer_name = '%s'" % customer_name
        db_cursor = database_connection.cursor()
        db_cursor.execute(db_all_customer_query)
        result = db_cursor.fetchall()

        if result is not None:
            return result
        else:
            return None

    except (pymssql.Error, pymssql.Warning) as error_message:
        logfunction.error_log(error_message)
        return 0
    finally:
        database_connection.close()
