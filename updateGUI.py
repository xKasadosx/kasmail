import sys
from PyQt5.QtWidgets import QDialog, QLineEdit
from qtpy import QtWidgets
from ui.update import Ui_update
import updatefunctions as updatefunctions


class UpdateWindow(QtWidgets.QMainWindow, Ui_update):
    def __init__(self, parent=None):
        super(UpdateWindow, self).__init__()
        self.setupUi(self)
        self.setWindowTitle("")
        update_label = self.updateLabel
        self.updateButtonClose.clicked.connect(self.close_window)

        updatefunctions.set_text(update_label)

    def close_window(self):
        self.close()
