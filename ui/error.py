# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui\error.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_errorWindow(object):
    def setupUi(self, errorWindow):
        errorWindow.setObjectName("errorWindow")
        errorWindow.resize(328, 136)
        self.errorLabel = QtWidgets.QLabel(errorWindow)
        self.errorLabel.setGeometry(QtCore.QRect(20, 10, 301, 61))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.errorLabel.setFont(font)
        self.errorLabel.setObjectName("errorLabel")
        self.errorButtonSend = QtWidgets.QPushButton(errorWindow)
        self.errorButtonSend.setGeometry(QtCore.QRect(20, 80, 111, 41))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.errorButtonSend.setFont(font)
        self.errorButtonSend.setObjectName("errorButtonSend")
        self.errorButtonNotSend = QtWidgets.QPushButton(errorWindow)
        self.errorButtonNotSend.setGeometry(QtCore.QRect(200, 80, 111, 41))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.errorButtonNotSend.setFont(font)
        self.errorButtonNotSend.setObjectName("errorButtonNotSend")

        self.retranslateUi(errorWindow)
        QtCore.QMetaObject.connectSlotsByName(errorWindow)

    def retranslateUi(self, errorWindow):
        _translate = QtCore.QCoreApplication.translate
        errorWindow.setWindowTitle(_translate("errorWindow", "Dialog"))
        self.errorLabel.setText(_translate("errorWindow", "Ein Fehler ist aufgetreten. \n"
"WollenSie die Fehlermeldung senden?"))
        self.errorButtonSend.setText(_translate("errorWindow", "Senden"))
        self.errorButtonNotSend.setText(_translate("errorWindow", "Nicht senden"))
