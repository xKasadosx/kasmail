import sys
from PyQt5.QtWidgets import QDialog
from qtpy import QtWidgets
import databasefunction as databasefunction
from ui.databasecustomers import Ui_Databasecustomers
import databasefunction as databasefunction


def clear():
    databasefunction.clear_database_gui(object_list)


def find_all():
    tableView.setRowCount(0)
    databasefunction.find_all(object_list[1], tableView)


def insert_customer_GUI():
    databasefunction.insert(object_list, customer_infos, tabSearchLabelFound)


def update_customer_GUI():
    databasefunction.update(object_list, customer_infos, tabSearchLabelFound)


def delete_customer_GUI():
    databasefunction.delete(object_list, customer_infos, tabSearchLabelFound)


def find_customer_GUI():
    databasefunction.find(object_list, tabSearchLabelFound)


class DatabasecustomerWindow(QtWidgets.QMainWindow, Ui_Databasecustomers):
    def __init__(self, parent = None):
        super(DatabasecustomerWindow,self).__init__()
        self.setupUi(self)
        self.setWindowTitle("Kunden Datenbank")
    
        self.tabSearchButtonClear.clicked.connect(clear)
        self.tabSearchButtonSearch.clicked.connect(find_customer_GUI)
        self.tabSearchButtonClose.clicked.connect(self.close_window_db_GUI)
        self.tabSearchButtonInsert.clicked.connect(insert_customer_GUI)
        self.tabSearchButtonUpdate.clicked.connect(update_customer_GUI)
        self.tabSearchButtonDelete.clicked.connect(delete_customer_GUI)
        self.tabSearchButtonAllcustomer.clicked.connect(find_all)
        self.tabOverviewTable.doubleClicked.connect(self.get_selected_item)
        global tab
        tab = self.tab
        self.tabOverviewTable
        global tableView
        tableView = self.tabOverviewTable
        tableView.setColumnWidth(0,90)
        tableView.setColumnWidth(1,200)
        tableView.setColumnWidth(2,150)
        tableView.setColumnWidth(3,140)
        tableView.setColumnWidth(5,58)
        global tabSearchLabelFound
        tabSearchLabelFound = self.tabSearchLabelFound
        global customer_infos
        customer_id = ""
        customer_name =" "
        customer_mail_one = ""
        customer_mail_two = ""
        customer_mail_three = ""
        customer_max_pdfs = ""
        customer_infos = [customer_id, customer_name, customer_mail_one, customer_mail_two, customer_mail_three, customer_max_pdfs]
        global object_list
        object_list = [self.tabSearchLineCustomerID, self.tabSearchLineName, self.tabSearchLineMail1, self.tabSearchLineMail2, self.tabSearchLineMail3, self.tabSearchLinePDFSend]

    def get_selected_item(self):
        databasefunction.select_customer_data_column(tableView, object_list)
        self.tab.setCurrentIndex(0)

    def close_window_db_GUI(self):
        self.close()
